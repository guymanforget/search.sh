#!/bin/bash
function cache {
list=$(find -L . -name *.html)
echo Proccessing $(echo "$list" | wc -l) files
echo "$list" | while read line
do
cat "$line" | sed -n 's:<title>\(.*\)</title>:\1:Ip' | xargs -0 -I {} echo -n "$line	{}" >> cache.txt
unset list
done
}
function searchtitle {
cat -n cache.txt | cut -f 2- | sed 's:^ *::' | grep "$@"
}
function fzftitle {
cat -n cache.txt | sed 's:^ *::' | grep "^$(cat -n cache.txt | sed 's:^ *::' | cut -f 1,3- | fzf | cut -f 1)	" | cut -f 2
}
function searchall {
cat cache.txt | grep "$@"
}
function fzfall {
cat cache.txt | fzf
}
function searchdir {
cat cache.txt | cut -f 1 | grep "$@"
}
function fzfdir {
cat cache.txt | cut -f 1 | fzf
}
function linedir {
cat -n cache.txt | sed 's:^ *::' | grep ^$@ | cut -f 2
}
case $1 in 
	"cache")
	cache
	;;
	"searchtitle")
	searchtitle "${@:2}"
	;;
	"searchall")
	searchall "${@:2}" | cut -f 1
	;;
	"searchdir")
	searchdir "${@:2}"
	;;
	"fzftitle")
	fzftitle
	;;
	"fzfall")
	fzfall | cut -f 1
	;;
	"fzfdir")
	fzfdir
	;;
	"linedir")
	linedir "${@:2}"
	;;
	*)
	echo "cache, searchtitle, searchall, searchdir, fzftitle, fzfall, fzfdir, linedir. fzf and cache options don't require arguments."
esac
